#! /usr/bin/env python

import rospy
from fetch_moveit_config.fetch_commander import FetchCommander
import geometry_msgs.msg

if __name__ == "__main__":

    rospy.init_node('move_group_python_interface_tutorial', anonymous=True)
    traj_serv_object = FetchCommander()

    print("All Zeros ee Pose")
    ee_pose = geometry_msgs.msg.Pose()

    ee_pose.position.x = 0.9616499977481227
    ee_pose.position.y = 1.7153330350514874e-05
    ee_pose.position.z = 0.7860662143235629
    ee_pose.orientation.x = 0.005094929484692078
    ee_pose.orientation.y = -5.475560224745837e-05
    ee_pose.orientation.z = 9.29634914807148e-06
    ee_pose.orientation.w = 0.9999870192202236

    traj_serv_object.move_ee_to_pose(ee_pose)

    print(str(traj_serv_object.get_ee_pose()))
    print(str(traj_serv_object.get_ee_rpy()))

    print("All Zeros ee Pose - delta")
    ee_pose.position.x -= 0.1
    traj_serv_object.move_ee_to_pose(ee_pose)

    print(str(traj_serv_object.get_ee_pose()))
    print(str(traj_serv_object.get_ee_rpy()))