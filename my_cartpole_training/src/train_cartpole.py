#!/usr/bin/env python
import gym

from baselines import deepq
from openai_ros.task_envs.cartpole_stay_up import stay_up
import rospy
import rospkg
import os


def callback(lcl, _glb):
    # stop training if reward exceeds 199
    is_solved = lcl['t'] > 100 and sum(lcl['episode_rewards'][-101:-1]) / 100 >= 199
    return is_solved


def main():
    rospy.init_node('cartpole_training', anonymous=True, log_level=rospy.WARN)
    env = gym.make("CartPoleStayUp-v0")

    # get an instance of RosPack with the default search paths
    rospack = rospkg.RosPack()
    # list all packages, equivalent to rospack list
    rospack.list()
    # get the file path for rospy_tutorials
    path_pkg = rospack.get_path('my_cartpole_training')
    model_path = os.path.join(path_pkg, "cartpole_model.pkl")

    act = deepq.learn(
        env,
        network='mlp',
        lr=1e-3,
        total_timesteps=100000,
        buffer_size=50000,
        exploration_fraction=0.1,
        exploration_final_eps=0.02,
        print_freq=10,
        checkpoint_freq=10,
        callback=callback,
        load_path=model_path
    )


if __name__ == '__main__':
    main()
